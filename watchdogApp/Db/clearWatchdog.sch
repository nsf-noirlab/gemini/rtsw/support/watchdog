[schematic2]
uniq 1
[tools]
[detail]
[cell use]
use ba200tr 0 0 100 0 ba200tr#1
xform 0 800 600
use esubs 666 376 100 0 clearWatchdog$(name)
xform 0 816 648
p 752 711 100 0 1 SCAN:$(scan_mech)
p 611 348 100 0 1 SNAM:clearWatchdog
p 603 905 100 0 -1 DESC:Reset the watchdog timer
p 784 376 100 1024 0 name:$(IOC):clearWatchdog$(name)
p 372 861 100 0 1 def(INPA):$(task_mask)
[comments]
