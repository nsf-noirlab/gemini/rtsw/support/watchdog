/*
*   FILENAME
*   watchdog_impl.c
*
*   FUNCTION NAME(S)
*   clearWatchdog     - called from EPICS to reset the watchdog timer
*   watchdog_callback - if the timer triggers, call this
*   start_watchdog    - creates the RTEMS timer and starts it
*   stop_watchdog     - cancels and deletes the RTEMS timer
*
*/
/*
 * Revision 1.1  2018/07/27  rjc
 * First version
 *
 */

#include <subRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <epicsPrint.h>
#include <iocsh.h>
#include <rtems.h>
#include <bsp.h>
#include <dbScan.h>
#include <dbCommon.h>

// Declarations and static data

#define IDLE_TASK_ID 0x9010001

typedef enum {
	WD_OK     = 0,
	WD_ERROR  = 1,
	WD_ACTIVE = 2,

	WD_CPUT   = 4
} watchdog_status;

typedef struct {
	rtems_name name;
	unsigned period; // In milliseconds
	rtems_id id;
	rtems_timer_service_routine (*callback)(rtems_id, void *);
} Timer_Info;

#define REPORTING_LIMIT 10

typedef struct {
	const char *name;
	uint32_t ident;
	Timer_Info timer;
	unsigned active;
	unsigned long times_triggered_since_reset;
	unsigned long times_triggered_all;
//	struct dbCommon *currently_scanning;
} Watchdog_Data;

#define MILLISECONDS_TO_TICKS(ms) ((((unsigned long)rtems_clock_get_ticks_per_second()) * (ms)) / 1000)

static Watchdog_Data watchdog_tracking_list[] = {
	{ "10 second",  0x01, { rtems_build_name('W', 'T', 'M', '1'), 10100 } },
	{ "5 second",   0x02, { rtems_build_name('W', 'T', 'M', '2'),  5100 } },
	{ "2 second",   0x04, { rtems_build_name('W', 'T', 'M', '3'),  2100 } },
	{ "1 second",   0x08, { rtems_build_name('W', 'T', 'M', '4'),  1100 } },
	{ ".5 second",  0x10, { rtems_build_name('W', 'T', 'M', '5'),   520 } },
	{ ".2 second",  0x20, { rtems_build_name('W', 'T', 'M', '6'),   420 } },
	{ ".1 second",  0x40, { rtems_build_name('W', 'T', 'M', '7'),   120 } },
	{ ".05 second", 0x80, { rtems_build_name('W', 'T', 'M', '8'),    70 } },
	{NULL}
};

static Timer_Info heartbeat_timer = {
	rtems_build_name('W', 'D', 'H', 'T'),
	30000
};

static Timer_Info high_cpu_timer = {
	rtems_build_name('W', 'D', 'C', 'T'),
	250
};

#define ITERATE_WD_LIST(p) for (p = watchdog_tracking_list; p->name; p++)

static long clearWatchdog(struct subRecord *);
static watchdog_status start_watchdog(uint32_t);
static watchdog_status stop_watchdog(void);
static unsigned reset_the_timer(rtems_id, unsigned);
static rtems_timer_service_routine watchdog_callback(rtems_id, void*);
static rtems_timer_service_routine watchdog_generalized_callback(rtems_id, void*);
static void high_cpu_load_calc(Thread_Control*);
static rtems_timer_service_routine high_cpu_load_callback(rtems_id, void*);
static rtems_timer_service_routine heartbeat_callback(rtems_id, void*);
static REGISTRYFUNCTION external_callback = NULL;
static unsigned RETRY_FACTOR = 2;
static unsigned RETRY_DELAYS[] = { 1, 1, 2, 3, 5, 8, 13, 21, 34 };

#define LENGTH(array) (sizeof(array) / sizeof(array[0]))

#define ascii_num(num) ((char)(num) + '0')
#define print_year(p, num) { int i; int year = num; for (i = 3; i >= 0; i--, year /= 10) { *(p + i) = ascii_num(year % 10); } }
#define print_two_digit(p, offset, num) { *(p + (offset + 1)) = ascii_num(num % 10); *(p + offset) = (num < 10) ? '0' : ascii_num(num / 10); }

static void get_date_prefix(char *buffer, bool blank_if_no_stamp) {
	rtems_time_of_day tod;

	// There's way to use sprintf/snprintf at kernel land, so we'll
	// resort to a more artisanal method
	if (rtems_clock_get_tod(&tod) == RTEMS_SUCCESSFUL)
	{
		print_year(buffer, tod.year);
		buffer[4] = '/';
		print_two_digit(buffer, 5, tod.month);
		buffer[7] = '/';
		print_two_digit(buffer, 8, tod.day);
		buffer[10] = 'T';
		print_two_digit(buffer, 11, tod.hour);
		buffer[13] = ':';
		print_two_digit(buffer, 14, tod.minute);
		buffer[16] = ':';
		print_two_digit(buffer, 17, tod.second);
		buffer[19] = '\0';
	}
	else if (blank_if_no_stamp) {
		strcpy(buffer, "----/--/--T--:--:--");
	}
	else
		buffer[0] = '\0';
}

/*
 * Use this for a single timer
 */
static watchdog_status reset_the_timer(rtems_id wd_timer, unsigned retry_index) {
	unsigned seconds;
	static unsigned next_retry_index = 0;
	unsigned ret;

	if (retry_index == -1) {
		retry_index = next_retry_index;
	}

	if (retry_index >= LENGTH(RETRY_DELAYS)) {
		retry_index = LENGTH(RETRY_DELAYS) - 1;
		ret = LENGTH(RETRY_DELAYS);
	}
	else
		ret = retry_index;

	seconds = RETRY_DELAYS[retry_index] * RETRY_FACTOR;

	if (rtems_timer_fire_after(wd_timer, rtems_clock_get_ticks_per_second() * seconds,
				   watchdog_callback, NULL) != RTEMS_SUCCESSFUL)
	{
		printf("ERROR: Could not activate the watchdog timer\n");
		return -1;
	}

	next_retry_index = retry_index + 1;

	return ret;
}

static void watchdog_reset(Watchdog_Data *wd)
{
	rtems_timer_reset(wd->timer.id);
	if (wd->times_triggered_since_reset >= REPORTING_LIMIT) {
		errlogPrintf("Watchdog[%s] resuming reports. It had been triggered a total of %ld times\n", wd->name, wd->times_triggered_since_reset);
	}
	wd->times_triggered_since_reset = 0;
}

/*+
 *   Function name:
 *   clearWatchdog
 *
 *   Purpose:
 *   Perform the operations needed to prevent the watchdog from triggering.
 *
 *   Description:
 *   This command tries to obtain the RTEMS ID for the watchdog timer, and
 *   upon success, it will reset the timer, preventing it from triggering.
 *
 *   Invocation:
 *   clearWatchdog (rec)
 *
 *   Parameters: (">" input, "!" modified, "<" output)  
 *      (!)   rec   (struct subRecord*)  Pointer to sub record structure
 *
 *   EPICS input parameters:
 *     A: seconds between scans
 *
 *   EPICS output parameters:
 *
 *   Function value:
 *   (<)  status  (long) Return status, 0 = OK
 *
 *   External functions:
 *   rtems_timer_ident     (RTEMS)        Obtain the ID for a timer, given
 *   					  its name.
 *   rtems_timer_reset     (RTEMS)        When passed the ID of a timer
 *					  based on an interval, it will
 *					  reset the elapsed time to 0.
 *
 *   Deficiencies:
 *   No known problems with the function
 *
 *-
 */
long clearWatchdog(struct subRecord *rec) {
	Watchdog_Data *p;

	// Look up the timer and reset it, if it exists
	ITERATE_WD_LIST(p) {
		if ((p->ident == (unsigned)rec->a) && (p->active)) {
			watchdog_reset(p);
			break;
		}
	}

	return 0;
}

void watchdog_print_thread_status(Thread_Control *the_thread) {
	char date_prefix[20];

	get_date_prefix(date_prefix, true);
	printk("%s: %x - %x", date_prefix, the_thread->Object.id, the_thread->current_state);
	if (the_thread->Wait.id != 0) {
		printk(" (%x)\n", the_thread->Wait.id);
	} else {
		printk("\n");
	}
}

/*+
 *   Function name:
 *   watchdog_callback
 *
 *   Purpose:
 *   Perform some task when the watchdog timer triggers
 *
 *   Description:
 *   This routine will just print a message to the console letting the user
 *   know that something is preventing EPICS (or, at least, the record tasked
 *   with resetting the watchdog) from processing.
 *
 *   If an additional external callback has been defined, it will be invoked
 *   after printing the message.
 *
 *   Invocation:
 *   watchdog_callback (timer_id, &user_data)
 *
 *   Parameters: (">" input, "!" modified, "<" output)
 *      (>)   timer_id  (rtems_id)  The ID for the RTEMS time object
 *      (>)   user_data (void *)    Optional custom data prepared passed at the
 *                                  time of timer creation.
 *
 *   external functions:
 *   printk                 (RTEMS)       Print a message while in ISR/kernel mode.
 *   rtems_clock_get_tod    (RTEMS)       Obtain the time of the day according to the
 *					  OS internal clock.
 *   external_callback                    User provided
 *
 *
 *   Deficiencies:
 *   No known problems with the function
 *
 *   Function value:
 *   None returned
 *
 *-
 */
rtems_timer_service_routine watchdog_callback(rtems_id timer_id, void *user_data) {
	unsigned retry_index;
	char date_prefix[20];

	get_date_prefix(date_prefix, true);
	printk("%s: Watchdog: stall detected!!!\n", date_prefix);

	if (external_callback != NULL) {
		external_callback();
	}

	retry_index = reset_the_timer(timer_id, -1);
	if (retry_index != -1) {
		if (retry_index < LENGTH(RETRY_DELAYS)) {
			printk("Trying again after %d seconds\n", RETRY_DELAYS[retry_index] * RETRY_FACTOR);
		}
		else {
			printk("Max retries reached. Stopping the watchdog\n");
			rtems_timer_cancel(timer_id);
			printk("Printing out the current thread status\n");
			rtems_iterate_over_all_threads(watchdog_print_thread_status);
			//bsp_reset();
		}
	}
}

rtems_timer_service_routine watchdog_generalized_callback(rtems_id timer_id, void *user_data) {
	Watchdog_Data *wd = (Watchdog_Data *)user_data;
//	struct dbCommon *precord = wd->currently_scanning;
//	const char *record_name = precord != NULL ? precord->name : "none";
	char date_prefix[20];

	wd->times_triggered_all++;
	if ((wd->times_triggered_since_reset ++) < REPORTING_LIMIT) {
		get_date_prefix(date_prefix, true);
		printk("%s: Watchdog[%s]: stall detected!!!\n", date_prefix, wd->name);
		if (wd->times_triggered_since_reset == REPORTING_LIMIT) {
			printk("%s: Watchdog[%s]: won't report any longer\n", date_prefix, wd->name);
		}
	}

	rtems_timer_reset(timer_id);
}

/*
static void watchdog_scanlist_progress_callback(void *token, struct dbCommon *precord) {
	Watchdog_Data *wd = (Watchdog_Data *)token;

	wd->currently_scanning = precord;
	// Reset the timer at the beginning and the end of scanning the list
	if ((precord == NULL) && (wd->timer_id))
		rtems_timer_reset(wd->timer_id);
}
*/

static void watchdog_reset_tracking_list(void) {
	Watchdog_Data *p;

	ITERATE_WD_LIST(p) {
		/*
		if (p->active) {
			scanResetCallback(p->name);
			p->active = 0;
		}
		*/
		p->active = 0;
		p->times_triggered_since_reset = 0;
	}
}

static unsigned watchdog_set_tracking_list(uint32_t mask) {
	Watchdog_Data *p;
	unsigned activated = 0;

	ITERATE_WD_LIST(p) {
		/*
		if (!scanInstallCallback(p->name, p, watchdog_scanlist_progress_callback)) {
			errlogPrintf("Watchdog: could not find a scan list '%s'\n", p->name);
		}
		else {
			p->active = 1;
		}
		*/
		if (!(mask & p->ident)) {
			continue;
		}
		p->active = 1;
		activated++;
	}

	return activated;
}

static watchdog_status initialize_simple_timer(const char *name, Timer_Info *timer, rtems_timer_service_routine (*callback)(rtems_id, void*))
{
	rtems_id timer_id;

	if (rtems_timer_ident(timer->name, &timer_id) == RTEMS_SUCCESSFUL) {
		errlogPrintf("The %s was running already\n", name);

		return WD_ACTIVE;
	}

	if(rtems_timer_create(timer->name, &timer->id) != RTEMS_SUCCESSFUL) {
		errlogPrintf("ERROR: Could not create the %s\n", name);
		return WD_ERROR;
	}

	timer->callback = callback;

	return WD_OK;
}

static watchdog_status reset_simple_timer(Timer_Info *timer, void *arg)
{
	// Fire the watchdog timer after the period has passed
	if (rtems_timer_fire_after(timer->id, MILLISECONDS_TO_TICKS(timer->period), timer->callback, arg) != RTEMS_SUCCESSFUL)
	{
		rtems_timer_delete(timer->id);

		return WD_ERROR;
	}

	return WD_OK;
}

static watchdog_status destroy_timer(const char *name, Timer_Info *timer)
{
	rtems_id timer_id;

	if (rtems_timer_ident(timer->name, &timer_id) != RTEMS_SUCCESSFUL) {
		errlogPrintf("The %s does not seem to be running\n", name);
		return WD_OK;
	}

	if (rtems_timer_delete(timer_id) != RTEMS_SUCCESSFUL) {
		return WD_ERROR;
	}

	return WD_OK;
}

/*+
 *   Function name:
 *   start_watchdog
 *
 *   Purpose:
 *   Start the watchdog timer
 *
 *   Description:
 *   Initializes and fires the watchdog timer. If the timer exists already,
 *   print a message and return.
 *
 *   Invocation:
 *   start_watchdog (mask)
 *
 *   Parameters: (">" input, "!" modified, "<" output)
 *      (>)   mask   (uint32_t)  Bit mask specifying which scan groups to track
 *
 *   Function value:
 *   (<)  status  (watchdog_status) Return status
 *       WD_OK       Everything ok, or watchdog existed already
 *       WD_ERROR    The timer could not be created, or activated
 *
 *   external functions:
 *   rtems_timer_ident      (RTEMS)       Obtain the id for a timer, given
 *   					  its name.
 *   rtems_timer_create     (RTEMS)       Create a new timer object with
 *					  the given name.
 *   rtems_timer_fire_after (RTEMS)       Initiates the specified timer,
 *					  scheduling it to fire after a
 *					  certain number of ticks have passed.
 *   rtems_timer_delete     (RTEMS)       Cancel a timer and remove its object.
 *
 *   Deficiencies:
 *   No known problems with the function
 *
 *-
 */
watchdog_status start_watchdog(uint32_t mask) {
	// rtems_id timer_id;
	Watchdog_Data *p;
	int status = WD_OK;

	errlogPrintf("Received mask: %ld\n", mask);

	ITERATE_WD_LIST(p) {
		if (p->timer.id) {
			errlogMessage("Watchdog was running already\n");
			return WD_ACTIVE;
		}
	}

	if (watchdog_set_tracking_list(mask) == 0) {
		errlogMessage("Watchdog warning: no scan group will be tracked (zeroed mask)\n");
	}
	else {
		ITERATE_WD_LIST(p) {
			if (p->active) {
				watchdog_status status = initialize_simple_timer(p->name, &p->timer, watchdog_generalized_callback);

				if (status == WD_ERROR) {
					errlogPrintf("ERROR: Could not create the watchdog timer for '%s'\n", p->name);
					status |= WD_ERROR;
				}
				else if ((status == WD_OK) && (reset_simple_timer(&p->timer, p) != RTEMS_SUCCESSFUL))
				{
					errlogPrintf("ERROR: Could not activate the watchdog timer for '%s'\n", p->name);
					status |= WD_ERROR;
				}
			}
		}
	}

	if (initialize_simple_timer("high CPU load", &high_cpu_timer, high_cpu_load_callback) == WD_OK) {
		if (reset_simple_timer(&high_cpu_timer, NULL) != WD_OK) {
			errlogMessage("ERROR: Could not initialize the hich CPU load timer\n");
			status |= WD_ERROR;
		}
		else
			status |= WD_CPUT;
	}

        if (initialize_simple_timer("heartbeat monitor", &heartbeat_timer, heartbeat_callback) == WD_OK) {
                if (reset_simple_timer(&heartbeat_timer, NULL) != WD_OK) {
                        errlogMessage("ERROR: Could not initialize the heartbeat monitor\n");
                        status |= WD_ERROR;
                }
                else
			errlogMessage("Heartbeat monitor started");
                        status |= WD_CPUT;
        }



	return status;
}


/*+
 *   Function name:
 *   stop_watchdog
 *
 *   Purpose:
 *   Stop the watchdog timer
 *
 *   Description:
 *   Cancels and destroys the timer object. If there's no such object, print
 *   a message and return OK.
 *
 *   Invocation:
 *   stop_watchdog ()
 *
 *   Parameters: (">" input, "!" modified, "<" output)
 *
 *   Function value:
 *   (<)  status  (watchdog_status) Return status
 *       WD_OK       Everything ok, or watchdog did not exist
 *       WD_ERROR    The timer could not be deleted
 *
 *   external functions:
 *   rtems_timer_ident      (RTEMS)       Obtain the id for a timer, given
 *   					  its name.
 *   rtems_timer_delete     (RTEMS)       Cancel a timer and remove its object.
 *
 *   Deficiencies:
 *   No known problems with the function
 *
 *-
 */
watchdog_status stop_watchdog(void) {
	Watchdog_Data *p;

	if (destroy_timer("high CPU load", &high_cpu_timer) != WD_OK) {
		errlogMessage("There was something wrong when trying to destroy the high CPU load timer\n");
		high_cpu_timer.id = 0;
	}

	ITERATE_WD_LIST(p) {
		if (p->active && p->timer.id) {
			rtems_timer_cancel(p->timer.id);
		}
	}

	watchdog_reset_tracking_list();

	ITERATE_WD_LIST(p) {
		if (p->timer.id) {
			if (destroy_timer(p->name, &p->timer) != WD_OK) {
				errlogPrintf("There was something wrong when trying to remove the watchdog timer for '%s'", p->name);
			}
			p->timer.id = 0;
		}
	}

	return WD_OK;
}

// It shall be that 0 < HIGH_CPU_LOAD_FACTOR < 100
#define HIGH_CPU_LOAD_PERCENTAGE 50
#define MAX_THREADS_TO_SHOW  3
#define NS_IN_SEC 1000000000

static Thread_CPU_usage_t total_cpu_usage;
static Thread_CPU_usage_t high_cpu_threshold;
static int      number_of_high_threads;
static int      heartbeat_cont;

/*
 * Assumes that dest points to a positive usage, and that
 * overflow/underflow is not possible.
 */
static void Thread_CPU_usage_t_add_inplace(Thread_CPU_usage_t *dest, Thread_CPU_usage_t delta)
{
	dest->tv_sec += delta.tv_sec;
	dest->tv_nsec += delta.tv_nsec;

	while (dest->tv_nsec > NS_IN_SEC) {
		dest->tv_nsec -= NS_IN_SEC;
		dest->tv_sec += 1;
	}
}

static bool Thread_CPU_usage_t_gt(Thread_CPU_usage_t a, Thread_CPU_usage_t b)
{
	return (a.tv_sec > b.tv_sec) || ((a.tv_sec == b.tv_sec) && (a.tv_nsec >= b.tv_nsec));
}

static void Thread_CPU_usage_t_reset(Thread_CPU_usage_t *dest)
{
	dest->tv_sec = 0;
	dest->tv_nsec = 0;
}

void collect_total_cpu_load(Thread_Control *the_thread)
{
	Thread_CPU_usage_t_add_inplace(&total_cpu_usage, the_thread->cpu_time_used);
}

void high_cpu_load_calc(Thread_Control *the_thread)
{
	char date_prefix[20];

	if ((the_thread->Object.id != IDLE_TASK_ID) && (number_of_high_threads < MAX_THREADS_TO_SHOW))
		if (Thread_CPU_usage_t_gt(the_thread->cpu_time_used, high_cpu_threshold)) {
			get_date_prefix(date_prefix, true);
			printk("%s: Thread %x uses > %d%% of the CPU\n", date_prefix, the_thread->Object.id, (the_thread->cpu_time_used.tv_sec * 100 / total_cpu_usage.tv_sec));
			number_of_high_threads++;
		}

}

rtems_timer_service_routine high_cpu_load_callback(rtems_id timer_id, void *user_data)
{
	rtems_timer_reset(high_cpu_timer.id);
	Thread_CPU_usage_t_reset(&total_cpu_usage);
	Thread_CPU_usage_t_reset(&high_cpu_threshold);
	number_of_high_threads = 0;
	rtems_iterate_over_all_threads(collect_total_cpu_load);
	high_cpu_threshold.tv_sec = (total_cpu_usage.tv_sec * HIGH_CPU_LOAD_PERCENTAGE) / 100;
	rtems_iterate_over_all_threads(high_cpu_load_calc);
}

rtems_timer_service_routine heartbeat_callback(rtems_id timer_id, void *user_data)
{
	rtems_timer_reset(heartbeat_timer.id);	
	printk("RTEMS is alive since %d minutes\n", heartbeat_cont++/2);		
}

static void exampleWatchdogCallback(void) {
	errlogMessage("This callback function is executed as part of the watchdog being triggered!\n");
}

static const iocshArg watchdogStartArg0 = {"Scan Mask", iocshArgInt};
static const iocshArg * const watchdogStartArgs[] = { &watchdogStartArg0 };
static const iocshFuncDef watchdogStartFuncDef = {"watchdogStart", 1, watchdogStartArgs};

static const iocshFuncDef watchdogStopFuncDef = {"watchdogStop", 0, NULL};

static const iocshArg watchdogReportArg0 = {"Attention Level", iocshArgInt};
static const iocshArg * const watchdogReportArgs[] = { &watchdogReportArg0 };
static const iocshFuncDef watchdogReportFuncDef = {"watchdogReport", 1, watchdogReportArgs};
// static const iocshArg watchdogSetCallbackCbArg = {"callback", iocshArgString};
// static const iocshArg *const watchdogSetCallbackArgs[] = {&watchdogSetCallbackCbArg};
// static const iocshFuncDef watchdogSetCallbackFuncDef = {"watchdogSetCallback", 1, watchdogSetCallbackArgs};

static void watchdogStartFunc(const iocshArgBuf *args)
{
	start_watchdog(args[0].ival);
}

static void watchdogStopFunc(const iocshArgBuf *args)
{
	stop_watchdog();
}

static void watchdogReportFunc(const iocshArgBuf *args)
{
	int active = 0;
	int level = args[0].ival;
	Watchdog_Data *p;

	if (level == 0) {
		errlogMessage("    Name     Missed since last reset\n");
		errlogMessage("------------ -----------------------\n");
	}
	else {
		errlogMessage("    Name     Act Missed since last reset Since watchdog started\n");
		errlogMessage("------------ --- ----------------------- ----------------------\n");
	}

	ITERATE_WD_LIST(p) {
		if (level > 0) {
			errlogPrintf("%12s  %c  %23ld %22ld\n",
					p->name,
					p->active ? 'Y' : 'N',
					p->times_triggered_since_reset,
					p->times_triggered_all);
		}
		else if (p->active) {
			active ++;
			errlogPrintf("%12s %23ld\n", p->name, p->times_triggered_since_reset);
		}
	}

	if ((level == 0) && (active == 0)) {
		errlogMessage("No active watchdogs\n");
	}
}

/*
static void watchdogSetCallbackFunc(const iocshArgBuf *args)
{
	if (args[0].sval == NULL) {
		external_callback = NULL;
		errlogMessage("Watchdog: external callback unset\n");
	} else {
		REGISTRYFUNCTION rf = registryFunctionFind(args[0].sval);
		if (!rf) {
			errlogPrintf("Watchdog: Can't find exported function: %s\n", args[0].sval);
		} else {
			external_callback = rf;
			errlogPrintf("Watchdog: external callback set to: %s\n", args[0].sval);
		}
	}
}
*/

static void watchdogRegister(void) {
typedef struct {
	const char *name;
	uint32_t ident;
	Timer_Info timer;
	unsigned active;
	unsigned long times_triggered;
//	struct dbCommon *currently_scanning;
} Watchdog_Data;

	iocshRegister(&watchdogStartFuncDef, watchdogStartFunc);
	iocshRegister(&watchdogStopFuncDef, watchdogStopFunc);
	iocshRegister(&watchdogReportFuncDef, watchdogReportFunc);
//	iocshRegister(&watchdogSetCallbackFuncDef, watchdogSetCallbackFunc);
}

epicsExportRegistrar(watchdogRegister);
epicsRegisterFunction(clearWatchdog);
epicsRegisterFunction(exampleWatchdogCallback);

/*
typedef void (*SCANPROGRESSCALLBACK)(void *, struct dbCommon *);
epicsShareFunc void *scanInstallCallback(const char *, SCANPROGRESSCALLBACK);
*/
