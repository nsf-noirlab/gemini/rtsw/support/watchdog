# watchdog

EPICS support module to install RTEMS watchdog timers. There are a couple of features
implemented around them:

 * Detect situations where EPICS grinds to a halt, but the underlying operating
   system is still active.
 * Detect high CPU loads

Compatible with RTEMS >= 4.10, and EPICS using the OSI layer (tested under 3.14.12.7)

## How does it work

To detect the high CPU load, the module samples the individual CPU usage of each
system task every 250ms, and reports threads using the CPU more than a certain
hardcoded threshold (currently 50%). The IDLE task is always excluded, for obvious
reasons.

A message will be printed out for each task that has used more than the threshold.
up to 3 of them (to limit CPU use of the monitor itself), if the set threshold allows
for more than one task to go over the limit.

To detect EPICS scan stalls, the watchdog module (selectively) starts a number of
timers, each one assigned an ID and a period:

| ID   | Scan rate  | Period (in ms) |
| 0x01 | 10 second  |  10100 |
| 0x02 | 5 second   |   5100 |
| 0x04 | 2 second   |   2100 |
| 0x08 | 1 second   |   1100 |
| 0x10 | .5 second  |    520 |
| 0x20 | .2 second  |    220 |
| 0x40 | .1 second  |    120 |
| 0x80 | .05 second |     70 |

Note that the period is slightly over the scan rate. A process variable assigned to
the appropriate scan rate will reset the timer before it fires. In the case where
a PV does not match its deadline for too long, the timer will fire and a message
will be printed.

This can be used to find gross deadline misses, or totally halted systems. To
prevent filling the logs excessively, like in the case of a complete halt of the
EPICS framework, each deadline miss will be recorded only for 10 consecutive times.
The watchdog will print a message letting you know that this condition has been
reached, and in the event of a recovery, it will print out that it is resuming
normal operations.

See "IOC Shell Use" below for additional details.

The module offers two iocsh functions:

## Building

The module should be straightforward to build. Just check `configure/RELEASE`
and correct as needed.  The files under `configure` include some
Gemini-specific includes, but they should not make your build fail.

The following instructions make reference to Makefiles according to our
standard code layout. Please make changes as needed.

## Integration into your Project

Add the module to your `configure/RELEASE` as usual. Additionally, you will
want to add `App/src/Makefile` the following lines:

```
app_DBD += watchdog.dbd
app_LIBS += watchdog
```

Also, as mentioned above, the watchdog works by having PVs scanning at different
rates. Each one of them is monitored separately. To have them, two steps are
needed. First, add to your `App/Db/Makefile`:

```
DB_INSTALLS += clearWatchdog.db
```

This is actually a template DB, so as a second step we need to manually
instanciate separate copies of the PV. For that, We'll create a template file
like:

```
file db/clearWatchdog.db {
  pattern{IOC,name,task_mask,scan_mech}
  {tc2,10s,0x01,"10 second"}
  {tc2,5s,0x02,"5 second"}
  {tc2,2s,0x04,"2 second"}
  {tc2,1s,0x08,"1 second"}
  {tc2,_5s,0x10,".5 second"}
  {tc2,_2s,0x20,".2 second"}
  {tc2,_1s,0x40,".1 second"}
  {tc2,_05s,0x80,".05 second"}
}
```

To be loaded with (substitute the appropriate filename):

```
dbLoadTemplate("data/clearWatchdog.template")
```

In the template, the first column is the database prefix. In EPICS >= 3.15 this
column can be removed and the variable set at the command line as a "global
macro", but this is not possible for EPICS 3.14, and thus separate files must be
used for differente prefixes.

The second column is a suffix to add to the PV name, so that they're all
different. The third column is the ID of one watchdog timer (it is not a
mask; the value has to be an exact match). The last column is the scan rate
and it must be one of the values out of `menuScan`.

As an example, the first line will create a PV called `tc2:clearWatchdog10s`,
that will have scan every 10 seconds, with `tc2:clearWatchdog10s.A` being
assigned an input value of `0x01`, used by the generic watchdog support function,
to reset the adequate timer.

It is encouraged to have all of the PVs defined (the above entry can be used
verbatim), even if there's no actual watchdog to clear (because it's not enabled
by `watchdogStart`, see below).

## IOC Shell Use

The module exports three iocsh calls:


* `watchdogStart [ <mask> ]`
* `watchdogStop`
* `watchdogReport [ <level of interest> ]

`watchdogStart`'s mask shall be the number resulting from adding up the IDs of the
scan rates that need to be tracked, as shown in the table above. If no mask is
specified, then the scan task watchdog will be effectively disabled, and only the
High CPU monitor will be at work.

To test that the watchdog is working properly, you can just temporarily disable
processing for the record resetting the watchdog. Example:

```
tc2-sim-ioc> watchdogStart 0xff
Received mask: 255
tc2-sim-ioc> dbpf tc2:clearWatchdog1s.DISA 1
DBR_SHORT:          1         0x1
tc2-sim-ioc> 2019/01/09T02:54:48: Watchdog[1 second]: stall detected!!!
2019/01/09T02:54:50: Watchdog[1 second]: stall detected!!!
2019/01/09T02:54:51: Watchdog[1 second]: stall detected!!!
2019/01/09T02:54:52: Watchdog[1 second]: stall detected!!!
2019/01/09T02:54:53: Watchdog[1 second]: stall detected!!!
2019/01/09T02:54:54: Watchdog[1 second]: stall detected!!!
2019/01/09T02:54:55: Watchdog[1 second]: stall detected!!!
2019/01/09T02:54:56: Watchdog[1 second]: stall detected!!!
2019/01/09T02:54:57: Watchdog[1 second]: stall detected!!!
2019/01/09T02:54:58: Watchdog[1 second]: stall detected!!!
2019/01/09T02:54:58: Watchdog[1 second]: won't report any longer

tc2-sim-ioc>
tc2-sim-ioc> dbpf tc2:clearWatchdog1s.DISA 0
DBR_SHORT:          0         0x0
tc2-sim-ioc> Watchdog[1 second] resuming reports. It had been triggered 19 times
```
`watchdogReport` prints a brief of the current status for active watchdogs when
run at level of interest 0. A higher level of interest will cause it to print
current and historic status on each watchdog, active or not.
